/**
 * This utility function helps to slugify any text in a consistent and predictable way.
 * @param {string} text String you want to slugify
 */

function slugify(text) {
    
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
}

/**
 * This utility function helps to convert any text to a uppercase text.
 * @param {string} text string you want in uppercase
 */

function upperCase(text) {
    return text.toUpperCase();
}
